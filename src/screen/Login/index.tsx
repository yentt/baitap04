import React, {useEffect, useState, useMemo, useCallback, useRef} from 'react';
import {useGlobalContext} from '../../component/MyContext';
import {
  Alert,
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TextInput,
} from 'react-native';
import {MyButton, styleButton} from '../../component/MyButton';
import {MyTextInput} from '../../component/MyTextInput';

export function Login({navigation}: any) {
  const [userName, setUserName] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorUserName, setErrorUserName] = useState<string>();
  const [errorPassword, setErrorPassword] = useState<string>();

  const {copy, setCopy} = useGlobalContext();

  const refPassword = useRef<TextInput>(null);

  useEffect(() => {
    Alert.alert('Chao mung ban da den voi Yolo System');
    return () => {};
  }, []);

  const isValidField = useCallback((value?: string) => {
    return !!value && value.length > 0;
  }, []);

  useEffect(() => {
    if (isValidField(userName)) {
      setErrorUserName(undefined);
    }
  }, [userName, isValidField]);

  useEffect(() => {
    if (isValidField(password)) {
      setErrorPassword(undefined);
    }
  }, [password, isValidField]);

  const validateUser = useCallback(() => {
    console.log('validateUser', userName);
    if (isValidField(userName)) {
      setErrorUserName(undefined);
      return true;
    } else {
      setErrorUserName('Ban phai nhap user name');
      return false;
    }
  }, [isValidField, userName]);

  const validatePassword = useCallback(() => {
    if (isValidField(password)) {
      setErrorPassword(undefined);
      return true;
    } else {
      setErrorPassword('Ban phai nhap mat khau');
      return false;
    }
  }, [isValidField, password]);

  const isValidLogin = useMemo(() => {
    return isValidField(userName) && isValidField(password);
  }, [isValidField, password, userName]);

  const LoginButtonStyle = useMemo(() => {
    if (isValidLogin) {
      return [styleButton.button, styleButton.buttonLogin];
    } else {
      return [styleButton.button, styleButton.buttonDisable];
    }
  }, [isValidLogin]);

  const LoginButtonDisable = useMemo(() => {
    if (isValidLogin) {
      return false;
    } else {
      return true;
    }
  }, [isValidLogin]);

  const handleSubmit = () => {
    if (
      !!userName &&
      userName.length > 0 &&
      !!password &&
      password.length > 0
    ) {
      Alert.alert(
        'Xin chao ' + userName + ' ban da dang nhap thanh cong vao he thong',
      );
      setCopy(userName);
      navigation.navigate('Home');
    }
  };

  const validateLogin = useCallback(() => {
    const isValidUser = validateUser();
    const isValidPassword = validatePassword();
    if (isValidUser && isValidPassword) {
      // Alert.alert(
      //   'Xin chao ' + userName + ' ban da dang nhap thanh cong vao he thong',
      // );
      // //navigation.navigate('Home', {user: userName});
      // () => setCopy('userName');
      // navigation.navigate('Home');
    }
  }, [navigation, userName, validatePassword, validateUser]);

  const focusPassword = useCallback(() => {
    refPassword?.current?.focus();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.paddingContent}>
        <Text style={styles.titleStyle}>Yolo System</Text>
        <MyTextInput
          error={errorUserName}
          value={userName}
          onChangeText={setUserName}
          style={styles.input}
          returnKeyType="next"
          onBlur={validateUser}
          onSubmitEditing={focusPassword}
          clearButtonMode="while-editing"
        />
        <MyTextInput
          error={errorPassword}
          value={password}
          onChangeText={setPassword}
          secureTextEntry
          style={styles.input}
          onBlur={validatePassword}
          ref={refPassword}
          clearButtonMode="while-editing"
        />
        <MyButton
          onPress={handleSubmit}
          style={LoginButtonStyle}
          disabled={LoginButtonDisable}
          buttonText="Login"
        />
        <Text style={styles.titleStyle}>Or</Text>
        <MyButton
          style={[styleButton.button, styleButton.buttonFacebook]}
          buttonText="Facebook"
        />
        <MyButton
          style={[styleButton.button, styleButton.buttonGoogle]}
          buttonText="Google"
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  paddingContent: {
    borderColor: 'black',
    borderWidth: 1,
    paddingBottom: 15,
    marginHorizontal: 5,
  },
  titleStyle: {
    padding: 5,
    color: 'green',
    fontWeight: 'bold',
    fontSize: 50,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  input: {
    padding: 10,
    height: 40,
    marginHorizontal: 15,
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 10,
  },
});
