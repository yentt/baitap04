import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useGlobalContext} from '../component/MyContext';

import {createDrawerNavigator} from '@react-navigation/drawer';

import {About} from './About';

import {MyButton} from '../component/MyButton';

function HomeScreen({navigation}: any) {
  const {copy} = useGlobalContext();
  const {data, setData} = useGlobalContext();

  function moveCategory({route}: any) {
    setData(route);
    navigation.navigate('Category');
  }

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Xin chào {copy}</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <MyButton buttonText="Story" onPress={() => moveCategory('Story')} />
        <MyButton buttonText="Music" onPress={() => moveCategory('Music')} />
        <MyButton
          buttonText="Picture"
          onPress={() => moveCategory('Picture')}
        />
      </View>
      <View>
        <Text style={styles.title}>{data}</Text>
      </View>
    </View>
  );
}

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        options={{
          drawerIcon: () => <Icon name="home" color="black" size={25} />,
        }}
        name="Home"
        component={HomeScreen}
      />
      <Drawer.Screen
        options={{
          drawerIcon: () => <Icon name="info" color="black" size={25} />,
        }}
        name="About"
        component={About}
      />
    </Drawer.Navigator>
  );
}

export function Home() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="home" color="black" size={25} />,
        }}
        name="HomeDraw"
        component={MyDrawer}
      />
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="info" color="black" size={25} />,
        }}
        name="About"
        component={About}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 25,
  },
});
