import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useGlobalContext} from '../component/MyContext';
import {MyButton} from '../component/MyButton';

export function Category({navigation}: any) {
  const {data} = useGlobalContext();
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{data}</Text>
      <MyButton
        buttonText="Come Back Home"
        onPress={() => {
          navigation.navigate('Home');
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 25,
  },
});
