import {createContext, useContext} from 'react';
export type GlobalContent = {
  copy: string;
  setCopy: (c: string) => void;
  data: any;
  setData: (d: string) => void;
};
export const MyGlobalContext = createContext<GlobalContent>({
  copy: 'userName', // set a default value
  setCopy: () => {},
  data: 'data', // set a default value
  setData: () => {},
});
export const useGlobalContext = () => useContext(MyGlobalContext);
