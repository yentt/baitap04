import React from 'react';
import {
  TouchableOpacity,
  TouchableOpacityProps,
  Text,
  ViewStyle,
  TextStyle,
  StyleSheet,
} from 'react-native';

export type Props = TouchableOpacityProps & {
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
  buttonText?: string;
  customStyles?: ViewStyle;
};

export const MyButton: React.FC<Props> = ({
  textStyle = styleButton.textStyle,
  buttonStyle = styleButton.button,
  buttonText = 'Button',
  ...others
}) => {
  return (
    <TouchableOpacity style={[buttonStyle]} {...others}>
      <Text style={textStyle}>{buttonText}</Text>
    </TouchableOpacity>
  );
};
export const styleButton = StyleSheet.create({
  button: {
    padding: 10,
    marginHorizontal: 15,
    marginTop: 10,
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 1,
  },
  textStyle: {
    fontWeight: '700',
    textAlign: 'center',
    color: 'black',
  },
  buttonLogin: {
    backgroundColor: 'purple',
  },
  buttonFacebook: {
    backgroundColor: 'green',
  },
  buttonGoogle: {
    backgroundColor: 'red',
  },
  buttonDisable: {
    backgroundColor: '#b76b2d',
  },
});
export default MyButton;
