import * as React from 'react';
import {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {MyGlobalContext} from './src/component/MyContext';

import {Login} from './src/screen/Login/index';
import {Home} from './src/screen/Home';
import {Category} from './src/screen/Category';

const Stack = createStackNavigator();

function App() {
  const [copy, setCopy] = useState<string>('');
  const [data, setData] = useState<string>('');
  return (
    <MyGlobalContext.Provider value={{copy, setCopy, data, setData}}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Category" component={Category} />
        </Stack.Navigator>
      </NavigationContainer>
    </MyGlobalContext.Provider>
  );
}

export default App;
